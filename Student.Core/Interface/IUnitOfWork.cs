﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student.Core.Interface
{
    public interface IUnitOfWork
    {
        IStudent<T> RepositoryFor<T>() where T : class;
        void Save();
    }
}
