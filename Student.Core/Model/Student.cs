﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Student.Core.Model
{
    public class Student : BaseClass
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid StudentID { get; set; }

        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string Middlename { get; set; }
        public string HomeAddress { get; set; }
        public string FamilyAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string ParentPhoneNumber { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string ClassName { get; set; }

        public Guid SchoolClassSectionKey { get; set; }
        public string EmailAddress { get; set; }
        public string SchoolUniqueNo { get; set; }
        public bool ApproveStudent { get; set; }
        public string Password { get; set; }
        public bool IsRegistered { get; set; }
        public int DepartmentID { get; set; }
        public bool Discount { get; set; }

        //relationships

        public virtual ICollection<OldSchoolInformation> OldSchoolInformations { get; set; }
        public virtual ICollection<StudentArchieve> StudentArchieves { get; set; }

    }
}