﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Student.Core.Model
{
    public class OldSchoolInformation : BaseClass
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid OldSchoolInformationID { get; set; }
      
      
        public string OldSchoolName { get; set; }
        public string OldSchoolAddress { get; set; }
        public string OldClassWhenLeft { get; set; }
        public string ReasonsForLeaving { get; set; }
        public string OtherInformation { get; set; }
     
    
        //relationships
        public int StudentID { get; set; }
        public virtual Student Students { get; set; }

    }
}