﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Student.Core.Model
{
    public class StudentImage : BaseClass
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid StudentImageID { get; set; }      
        public Guid StudentID { get; set; }
        public string ImageName { get; set; }
        public string ImageUrl { get; set; }
       
        public bool CoverImage { get; set; }
        public bool CurrentImage { get; set; }
       
        public virtual Student Students { get; set; }

    }
}