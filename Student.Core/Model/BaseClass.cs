﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Student.Core.Model
{
    public class BaseClass
    {
        public DateTime DateCreated { get; set; }
        public bool IsDelete { get; set; }
        public Guid Key { get; set; }
        public BaseClass()
        {
            DateCreated = DateTime.Now;
            IsDelete = false;
            Key = Guid.NewGuid();       
        }
    }
}