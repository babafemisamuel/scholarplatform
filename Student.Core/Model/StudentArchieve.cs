﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Student.Core.Model
{
    public class StudentArchieve : BaseClass
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int StudentArchieveID { get; set; }
       
        public Guid StudentID { get; set; }
        public string ArchiveTitle { get; set; }
        public string ArchiveName { get; set; }
        public string ArchiveFileType { get; set; }
        public string ArchiveFile { get; set; }      

        //relationship
        public virtual Student Students { get; set; }

    }
}