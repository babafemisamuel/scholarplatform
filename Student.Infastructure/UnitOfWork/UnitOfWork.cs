﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Student.Core.Interface;
using Student.Infastructure.Model;
using Student.Infrastructure.Repository;

namespace Student.Infrastructure.UnitOfWork
{
    public class UnitOfWork:IUnitOfWork
    {
       private readonly StudentModel _db= new StudentModel();
        public IStudent<T> RepositoryFor<T>() where T : class
        {
            return new StudentRepository<T>(_db);
        }
       
        public void Save()
        {
            _db.SaveChanges();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
                this.disposed = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

      
    }
}