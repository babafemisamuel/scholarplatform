﻿
using Student.Infastructure.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Student.Infrastructure.Repository
{
    public class StudentRepository<TEntity> : Student.Core.Interface.IStudent<TEntity> where TEntity : class
    {
        StudentModel std = new StudentModel();

        protected readonly StudentModel db;
        public StudentRepository(StudentModel db)
        {
            this.db = db;
        }
        public void Add(TEntity entity)
        {
            db.Set<TEntity>().Add(entity);
        }
        public void GetWithInclude(TEntity entity)
        {
            db.Set<TEntity>().Include("");
        }
        public void AddRange(IEnumerable<TEntity> entities)
        {
            db.Set<TEntity>().AddRange(entities);
            //throw new NotImplementedException();
        }

        public void Edit(TEntity entity)
        {
            db.Entry(entity).State = EntityState.Modified;
            // throw new NotImplementedException();
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return db.Set<TEntity>().Where(predicate);
        }

        public TEntity FindByID(Func<TEntity, bool> predicate)
        {
           return db.Set<TEntity>().FirstOrDefault(predicate);
        }
        public TEntity Get(int id)
        {
            return db.Set<TEntity>().Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return db.Set<TEntity>().ToList();
        }

        public void Remove(TEntity entity)
        {
            db.Set<TEntity>().Remove(entity);
            //throw new NotImplementedException();
        }

        public void RemoveRange(IEnumerable<TEntity> entity)
        {
            db.Set<TEntity>().RemoveRange(entity);
        }

      
    }
}