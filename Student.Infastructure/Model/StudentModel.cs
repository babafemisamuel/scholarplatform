﻿using Student.Core.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Student.Infastructure.Model
{
    public class StudentModel : DbContext
    {
        public StudentModel():base("name=StudentModel")
        {

        }
        public DbSet<StudentImage> StudentImages { get; set; }
        public DbSet<Core.Model.Student> Students { get; set; }
        public DbSet<StudentArchieve> StudentArchieves { get; set; }
        public DbSet<OldSchoolInformation> OldSchoolInformations { get; set; }
    }
}