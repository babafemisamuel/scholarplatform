﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ScholarPlatform.Startup))]
namespace ScholarPlatform
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
