﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Student.Presentation.Startup))]
namespace Student.Presentation
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
